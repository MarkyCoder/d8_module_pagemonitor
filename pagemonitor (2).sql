-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2017 at 02:25 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `churchdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `pagemonitor`
--

CREATE TABLE IF NOT EXISTS `pagemonitor` (
  `monitorID` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `suburb` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `from_link` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `memberID` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(70) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `visitDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`monitorID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pagemonitor`
--

INSERT INTO `pagemonitor` (`monitorID`, `country`, `state`, `suburb`, `user_ip`, `from_link`, `memberID`, `page`, `user_agent`, `visitDate`) VALUES
(1, 'Phil', 'NCR', 'Phil', 'NCR', 'Phil', 'NCR', 'Phil', 'NCR', '2017-06-26 23:05:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
