<?php

namespace Drupal\page_monitor\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Random;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Database\Database;
/**
 * A handler to provide a field that is completely custom by the administrator.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("page_monitor")
 */
class PageMonitor extends FieldPluginBase {

    /**
     * {@inheritdoc}
     */
    public function usesGroupBy() {
        return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    public function query() {
        // Do nothing -- to override the parent query.
    }

    /**
     * {@inheritdoc}
     */
    protected function defineOptions() {
        $options = parent::defineOptions();

        $options['hide_alter_empty'] = ['default' => FALSE];
        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state) {
        parent::buildOptionsForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values) {


        // render PHP code to monitor visitor's location on every page

        //Clear render cache
        db_query("DELETE FROM {cache_render};");
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();

        $current_uri = \Drupal::request()->getRequestUri();
        $memberID =  \Drupal::currentUser()->getAccountName();

        $this->monitorPageVisit( $current_uri,$memberID);

    }

    public function monitorPageVisit($page,$memberID ) {

        $user_ip = getenv('REMOTE_ADDR');

        if (isset($user_ip)) {



            $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

            $sub = trim($geo["geoplugin_city"]);
            $state = trim($geo["geoplugin_regionName"]);
            $country = trim($geo["geoplugin_countryName"]);

        } else {
            $user_ip = 'None'; // show failure message
            $sub = 'not detected';
            $state = 'not detected';
            $country = 'not detected';
        }

        if (isset($_SERVER['HTTP_USER_AGENT'])) {

            $user_agent = $_SERVER['HTTP_USER_AGENT'];

        } else {
            $user_agent = 'Not detected';
        }



        if (isset($_SERVER['HTTP_REFERER'])) {
            $ref_url = $_SERVER['HTTP_REFERER']; //get referrer

            //$deny = array("top1-seo1", "pizza-imperia", "333.333.333");

            // redirecting spam referrals
            //if (in_array ($from, $deny)) {
            //header("location: http://www.google.com/");
            //exit();
            //


        }else{
            $ref_url = 'No referrer'; // show failure message
        }//end else no referer set



        db_insert('pagemonitor')
            ->fields(array (
                'country'=> $country ,
                'state'=> $state,
                'suburb'=> $sub,
                'user_ip'=> $user_ip,
                'from_link'=> $ref_url,
                'memberID'=> $memberID,
                'page'=> $page,
                'user_agent'=>  $user_agent

            ))
            ->execute();



    }//monitorPageVisit

}
